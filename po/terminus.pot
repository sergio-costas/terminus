# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the terminus package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: terminus\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-02 23:23+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/base.vala:84
msgid "Cancel"
msgstr ""

#: src/keybinding.vala:71 src/terminal.vala:165
msgid "New window"
msgstr ""

#: src/keybinding.vala:72 src/terminal.vala:160
msgid "New tab"
msgstr ""

#: src/keybinding.vala:73
msgid "Next tab"
msgstr ""

#: src/keybinding.vala:74
msgid "Previous tab"
msgstr ""

#: src/keybinding.vala:75
msgid "Show guake terminal"
msgstr ""

#: src/keybinding.vala:76
msgid "Copy text into the clipboard"
msgstr ""

#: src/keybinding.vala:77
msgid "Paste text from the clipboard"
msgstr ""

#: src/keybinding.vala:78
msgid "Move focus to the terminal on the left"
msgstr ""

#: src/keybinding.vala:79
msgid "Move focus to the terminal on the right"
msgstr ""

#: src/keybinding.vala:80
msgid "Move focus to the terminal above"
msgstr ""

#: src/keybinding.vala:81
msgid "Move focus to the terminal below"
msgstr ""

#: src/keybinding.vala:82
msgid "Make font bigger"
msgstr ""

#: src/keybinding.vala:83
msgid "Make font smaller"
msgstr ""

#: src/keybinding.vala:84
msgid "Reset font size"
msgstr ""

#: src/keybinding.vala:85
msgid "Show menu"
msgstr ""

#: src/keybinding.vala:86 src/terminal.vala:149
msgid "Split horizontally"
msgstr ""

#: src/keybinding.vala:87 src/terminal.vala:153
msgid "Split vertically"
msgstr ""

#: src/keybinding.vala:88
msgid "Close the active tile"
msgstr ""

#: src/keybinding.vala:89
msgid "Close the active tab"
msgstr ""

#: src/keybinding.vala:90 src/terminal.vala:142
msgid "Select all"
msgstr ""

#: src/notetab.vala:89
msgid "This tab has running processes inside."
msgstr ""

#: src/notetab.vala:90 src/window.vala:96
msgid "Closing it will kill them."
msgstr ""

#: src/notetab.vala:91
msgid "Close tab"
msgstr ""

#: src/palete.vala:120 src/palete.vala:178
#, c-format
msgid "Error: palette file %s has unrecognized content at line %d\n"
msgstr ""

#: src/palete.vala:136
#, c-format
msgid ""
"Error: palette file %s has opens a bracket at line %d without closing it\n"
msgstr ""

#: src/palete.vala:151
#, c-format
msgid "Error: palette file %s has an unrecognized color at line %d\n"
msgstr ""

#: src/palete.vala:163
#, c-format
msgid "Warning: palette file %s has more than 16 colors\n"
msgstr ""

#: src/palete.vala:190
#, c-format
msgid "Error: Palette file %s has less than 16 colors\n"
msgstr ""

#: src/palete.vala:194
#, c-format
msgid "Error: Palette file %s has no palette name\n"
msgstr ""

#: src/palete.vala:198
#, c-format
msgid "Error: Palette file %s has text_fg color but not text_bg color\n"
msgstr ""

#: src/palete.vala:202
#, c-format
msgid "Error: Palette file %s has text_bg color but not text_fg color\n"
msgstr ""

#: src/params.vala:119
#, c-format
msgid ""
"Parameter '%s' unknown.\n"
"\n"
msgstr ""

#: src/params.vala:128
#, c-format
msgid ""
"The '%s' parameter requires a path after it.\n"
"\n"
msgstr ""

#: src/params.vala:136
#, c-format
msgid ""
"The '%s' parameter requires a command after it.\n"
"\n"
msgstr ""

#: src/params.vala:145
msgid ""
"Usage:\n"
"terminus [OPTION...] [-- COMMAND ...]\n"
"\n"
"Help commands:\n"
"  -h, --help                    show this help\n"
"  -v, --version                 show version\n"
"\n"
"Options:\n"
"  -x, --execute, --             launches a new Terminus window and execute "
"the remainder of the command line inside the terminal\n"
"  -e, --command=STRING          launches a new Terminus window and execute "
"the argument inside the terminal\n"
"  --working-directory=DIRNAME   sets the terminal directory to DIRNAME\n"
"  --no-window                   launch Terminus but don't open a window\n"
"  --nobindkey                   don't try to bind the Quake-mode key (useful "
"for gnome shell)\n"
msgstr ""

#: src/settings.vala:149
#, c-format
msgid "Version %s"
msgstr ""

#: src/settings.vala:327
msgid "Action"
msgstr ""

#: src/settings.vala:328
msgid "Key"
msgstr ""

#: src/terminal.vala:130
msgid "Copy"
msgstr ""

#: src/terminal.vala:135
msgid "Paste"
msgstr ""

#: src/terminal.vala:172
msgid "Preferences"
msgstr ""

#: src/terminal.vala:179
msgid "Close"
msgstr ""

#: src/terminal.vala:507
msgid "This terminal has a process running inside."
msgstr ""

#: src/terminal.vala:508
msgid "Closing it will kill the process."
msgstr ""

#: src/terminal.vala:509
msgid "Close terminal"
msgstr ""

#: src/terminus.vala:66
#, c-format
msgid "Version %s\n"
msgstr ""

#: src/terminus.vala:105
msgid "Custom colors"
msgstr ""

#: src/window.vala:95
msgid "This window has running processes inside."
msgstr ""

#: src/window.vala:97
msgid "Close window"
msgstr ""

#: data/interface/properties.ui:22
msgid "Block"
msgstr ""

#: data/interface/properties.ui:26
msgid "Double T"
msgstr ""

#: data/interface/properties.ui:30
msgid "Underscore"
msgstr ""

#: data/interface/properties.ui:80
msgid "Enable guake mode"
msgstr ""

#: data/interface/properties.ui:99
msgid "Cursor shape:"
msgstr ""

#: data/interface/properties.ui:128
msgid "Terminal bell"
msgstr ""

#: data/interface/properties.ui:144
msgid "Custom font:"
msgstr ""

#: data/interface/properties.ui:161
msgid "Custom command shell:"
msgstr ""

#: data/interface/properties.ui:180
msgid "Use custom shell"
msgstr ""

#: data/interface/properties.ui:196
msgid "Pointer autohide"
msgstr ""

#: data/interface/properties.ui:214
msgid "General"
msgstr ""

#: data/interface/properties.ui:235 data/interface/properties.ui:484
msgid "Built-in schemes:"
msgstr ""

#: data/interface/properties.ui:288 data/interface/properties.ui:800
msgid "Text"
msgstr ""

#: data/interface/properties.ui:300 data/interface/properties.ui:812
msgid "Background"
msgstr ""

#: data/interface/properties.ui:324
msgid "Default color:"
msgstr ""

#: data/interface/properties.ui:333
msgid "Bold color:"
msgstr ""

#: data/interface/properties.ui:347
msgid "Cursor color:"
msgstr ""

#: data/interface/properties.ui:361
msgid "Highlight color:"
msgstr ""

#: data/interface/properties.ui:433
msgid "<b>Text and background colors</b>"
msgstr ""

#: data/interface/properties.ui:447
msgid "<b>Palette</b>"
msgstr ""

#: data/interface/properties.ui:516
msgid "Colors palete:"
msgstr ""

#: data/interface/properties.ui:763
msgid "<b>Top bar</b>"
msgstr ""

#: data/interface/properties.ui:825
msgid "Focused terminal:"
msgstr ""

#: data/interface/properties.ui:837
msgid "Inactive terminal:"
msgstr ""

#: data/interface/properties.ui:909
msgid "Colors"
msgstr ""

#: data/interface/properties.ui:935
msgid "Scrollback lines:"
msgstr ""

#: data/interface/properties.ui:957
msgid "0"
msgstr ""

#: data/interface/properties.ui:967
msgid "Infinite Scrollback"
msgstr ""

#: data/interface/properties.ui:1014
msgid "Scroll on output"
msgstr ""

#: data/interface/properties.ui:1028
msgid "Scroll on keystroke"
msgstr ""

#: data/interface/properties.ui:1049
msgid "Scrolling"
msgstr ""

#: data/interface/properties.ui:1077
msgid "Keybindings"
msgstr ""

#: data/interface/properties.ui:1097
msgid "Terminus"
msgstr ""

#: data/interface/properties.ui:1120
msgid ""
"A tiled terminal emulator.\n"
"\n"
"Written by Sergio Costas Rodríguez (rastersoft)\n"
"\n"
"http://www.rastersoft.com\n"
"rastersoft@gmail.com"
msgstr ""

#: data/interface/properties.ui:1143
msgid "About"
msgstr ""
